import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import Router from 'vue-router'
import Segmentator from './components/Segmentator.vue';
import Summarizator from './components/Summarizator.vue';
import Home from './components/Home.vue'

import VueClipboards from 'vue-clipboards'
Vue.use(VueClipboards);
Vue.use(Router);

Vue.config.productionTip = false
const router = new Router(
  {
    routes: [
      {
        path: '/',
        name:'home',
        component: Home
      },
      {
        path: '/segmentator',
        name:'segmentator',
        component: Segmentator
      },
      {
        path: '/summarizator',
        name:'summarizator',
        component: Summarizator
      }
    ]
  }
)

new Vue({
  vuetify,
  render: h => h(App),
  router
}).$mount('#app')
