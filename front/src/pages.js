let corePages=[
    {
      'name': 'Домашняя страница',
      'url': '/',
      'image': 'bar.jpg'
    },
    {
      'name': 'Сегментация',
      'url': 'segmentator',
      'image': 'segmentation.jpg'
    },
    {
      'name': 'Саммаризация',
      'url': 'summarizator',
      'image': 'summarization.jpg'
    }
  ];
export default corePages;