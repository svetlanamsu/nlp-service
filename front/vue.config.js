module.exports = {
  devServer:{
    proxy:{
      '^/api/':{
        target:'http://back:8081/', 
        secure: false,
        pathRewrite:{
          '/api/*': '/'
        }
      }
    }
  },
  transpileDependencies: [
    'vuetify'
  ]
}
