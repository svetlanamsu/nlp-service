from typing import Optional

from fastapi import FastAPI
from segmentation import api as segmentation_api
from summarization import api as summarization_api
import uvicorn

app = FastAPI()

app.include_router(segmentation_api.router)
app.include_router(summarization_api.router)


if __name__ == "__main__":
    # for debugging
    uvicorn.run(app, host="0.0.0.0", port=8082)