# TODO: in a work progress
import torch
import torch.nn as nn
import torch.nn.functional as F


class LSTMLower(nn.Module):
    """ LSTM over a Batch of variable length sentences, pool over
    each sentence's hidden states to get its representation.
    """

    def __init__(self, hidden_dim, emb_size, num_layers, bidir, drop_prob, method):
        super().__init__()
        self.drop_prob = drop_prob
        self.lstm = nn.LSTM(emb_size, hidden_dim, num_layers=num_layers,
                            bidirectional=bidir, batch_first=True, dropout=self.drop_prob)

        self.pool = Pool(method)

    def forward(self, batch):
        lstm_out, _ = self.lstm(batch)
        # Get lower output representation
        representation = self.pool(lstm_out)
        lower_output = torch.unsqueeze(torch.stack(representation), 0)
        return lower_output


class LSTMHigher(nn.Module):
    """ LSTM over the sentence representations from LSTMLower
    """

    def __init__(self, input_dim, hidden_dim, num_layers, bidir, drop_prob):
        super().__init__()

        self.drop = drop_prob

        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers=num_layers,
                            bidirectional=bidir, batch_first=True, dropout=self.drop)

    def forward(self, lower_output):
        # LSTM over sentence representations
        lstm_out, _ = self.lstm(lower_output)
        # Concatenate the sentences together for final scoring
        higher_output = torch.cat([sent.squeeze() for sent in lstm_out])

        return higher_output


class Score(nn.Module):
    """ Take outputs from encoder, produce probabilities for each
    sentence that it ends a segment.
    """

    def __init__(self, input_dim, hidden_dim, out_dim, drop_prob):
        super().__init__()

        self.score = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.ReLU(),
            nn.Dropout(drop_prob),
            nn.Linear(hidden_dim, hidden_dim),
            nn.ReLU(),
            nn.Dropout(drop_prob),
            nn.Linear(hidden_dim, out_dim),
        )

    def forward(self, higher_output):
        return self.score(higher_output)


class Pool:
    """ Given regrouped representations from batch, perform pooling over them
    using one of several methods.
    """

    def __init__(self, method):
        assert method in ['avg', 'last', 'max', 'sum', 'weighted'], 'Invalid method chosen.'
        self.method = eval('self._' + method)

    def __call__(self, *args):
        return self.method(*args)

    def _weighted(self, restored):
        """ Weighted sum  """
        weighted = [F.softmax(sent, dim=0) * sent for sent in restored]
        return self._sum(weighted)

    def _avg(self, restored):
        """ Average hidden states """
        return [sent.mean(dim=0) for sent in restored]

    def _last(self, restored):
        """ Take last token state representation """
        return [sent[-1, :] for sent in restored]

    def _max(self, restored):
        """ Maxpool token states """
        return [torch.max(sent, dim=0)[0] for sent in restored]

    def _sum(self, restored):
        """ Sum token states """
        return [sent.sum(dim=0) for sent in restored]


class HierarchicalLSTM(nn.Module):
    """ Super class for taking an input batch of sentences from a Batch
    and computing the probability whether they end a segment or not
    """

    def __init__(self, lstm_dim, emb_size, score_dim, bidir, num_layers=2, drop_prob=0.20, method='max'):
        super().__init__()

        # Compute input dimension size for LSTMHigher, Score
        num_dirs = 2 if bidir else 1
        input_dim = lstm_dim * num_dirs

        # Chain modules together to get overall model
        self.model = nn.Sequential(
            LSTMLower(lstm_dim, emb_size, num_layers, bidir, drop_prob, method),
            LSTMHigher(input_dim, lstm_dim, num_layers, bidir, drop_prob),
            Score(input_dim, score_dim, out_dim=2, drop_prob=drop_prob)
        )

    def forward(self, batch):
        return self.model(batch)

