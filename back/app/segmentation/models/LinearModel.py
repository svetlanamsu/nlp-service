import torch.nn as nn


class LinearModel(nn.Module):
    def __init__(self, embed_size, num_classes=2):
        super().__init__()

        self.cls = nn.Sequential(
            nn.Linear(embed_size, num_classes)
        )

    def forward(self, x):
        x = x.sum(dim=1)
        output = self.cls(x)
        return output