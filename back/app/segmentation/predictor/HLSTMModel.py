import torch

from .BaseModel import SimpleTorchModel
from ..models.HierarchicalLSTMModel import HierarchicalLSTM


class HLSTMModel(SimpleTorchModel):
    model_name = 'Иерархическая LSTM'
    def __init__(self):
        super().__init__()
        self.lstm_dim = 256
        self.score_dim=256
        self.bidir=True

    def load_model(self):
        model = HierarchicalLSTM(emb_size=self.embed_size, lstm_dim=self.lstm_dim, score_dim=self.score_dim, bidir=self.bidir)
        model.load_state_dict(torch.load(f'segmentation/predictor/source/models/hlstm_state.pt', map_location=self.device))
        self.instance = model

    def predict(self, text):
        out = super().predict(text=text)
        return list(out)
