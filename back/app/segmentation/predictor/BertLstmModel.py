import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy
from transformers import BertTokenizer, BertModel
from scipy.special import softmax
from razdel import sentenize

from .BaseModel import BaseModel


class LSTMTagger(nn.Module):
        
    def __init__(self, embedding_dim=768, hidden_dim=64, num_layers=2, dropout=0.1, bidirectional=True):
        super(LSTMTagger, self).__init__()
        self.hidden_dim = hidden_dim

        self.lstm = nn.LSTM(
                              input_size=embedding_dim,
                              hidden_size=hidden_dim,
                              num_layers=num_layers,
                              dropout=dropout,
                              bidirectional=bidirectional
                            )

        if bidirectional == True:
            self.hidden2tag = nn.Linear(hidden_dim * 2, 2)
        else:
            self.hidden2tag = nn.Linear(hidden_dim, 2)

    def forward(self, embeds):
        lstm_out, _ = self.lstm(embeds.view(len(embeds), 1, -1))
        tag_space = self.hidden2tag(lstm_out.view(len(embeds), -1))
        tag_scores = F.log_softmax(tag_space, dim=1)
        return tag_scores


class BertLSTMModel(BaseModel): 
    model_name = 'Контекстный Bert(BiLSTM)'
    def __init__(self):
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.path_bert = 'segmentation/predictor/source/models/bert_bilstm_model/bert_model/'
        self.path_bilstm = 'segmentation/predictor/source/models/bert_bilstm_model/bilstm_model/model_bilstm.pt'
        self.tokenizer = None
        self.model_bert = None
        self.model_bilstm = None

    def load_model(self):
        self.tokenizer = BertTokenizer.from_pretrained(self.path_bert)
        self.model_bert = BertModel.from_pretrained(self.path_bert).to(self.device)
        self.model_bilstm = LSTMTagger()
        self.model_bilstm.load_state_dict(torch.load(self.path_bilstm))

    def predict(self, text):
        sentences = self.split_sentences(text)
        embeddings = self._get_embeddings(sentences)
        predictions = self._get_labels(embeddings)

        parts = list()
        start = 0
        stop = len(text)
        for sentence, label in zip(sentences[:-1], predictions[:-1]):
            if label == 1:
                parts.append(text[start:sentence.stop])
                start = sentence.stop + 1
        parts.append(text[start:])
        
        return parts, predictions
    
    def _get_embeddings(self, sentences):
        sentences = [i.text for i in sentences]
        encoding = self.tokenizer(
                sentences,
                add_special_tokens=True,
                return_tensors='pt',
                padding='max_length',
                max_length=128,
                truncation=True
            )
        input_ids = encoding.input_ids.to(self.device)
        token_type_ids = encoding.token_type_ids.to(self.device)
        attention_mask = encoding.attention_mask.to(self.device)
        
        self.model_bert.eval()
        with torch.no_grad():
            outputs = self.model_bert(
                    input_ids=input_ids,
                    attention_mask=attention_mask,
                    token_type_ids=token_type_ids
                )
        embeddings = outputs.last_hidden_state[:,0,:].cpu().detach().numpy()
        return embeddings

    def _get_labels(self, embeddings):
        model = self.model_bilstm
        model.eval()
        
        inputs = torch.tensor(embeddings)
        tag_scores = model(inputs)
        
        x = tag_scores.detach().cpu().numpy()
        labels = [list(i).index(max(i)) for i in x]
        return labels

    @staticmethod
    def split_sentences(text):
        return list(sentenize(text))
