import torch
from transformers import BertTokenizer, BertForNextSentencePrediction
from scipy.special import softmax
from razdel import sentenize

from .BaseModel import BaseModel


class ContextBertModel(BaseModel): 
    model_name = 'Контекстный Bert'
    def __init__(self):
        self.path = 'segmentation/predictor/source/models/model_bert/'
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.width = 128
        self.tokenizer = None
        self.model = None
        self.threshold = 0.8

    def load_model(self):
        self.tokenizer = BertTokenizer.from_pretrained(self.path)
        self.model = BertForNextSentencePrediction.from_pretrained(self.path).to(self.device)

    def predict(self, text):
        sentences = self.split_sentences(text)
        sent_list = [i.text for i in sentences]
        predictions = self._get_predict_by_tokens(sent_list)
        
        parts = list()
        start = 0
        stop = len(text)
        for sentence, label in zip(sentences[:-1], predictions[:-1]):
            if label == 1:
                parts.append(text[start:sentence.stop])
                start = sentence.stop + 1
        parts.append(text[start:])
        
        return parts, predictions

    def _get_predict_by_tokens(self, sent_list):
        self.model.eval()
        end_list = []
        text_ind = []
        sent_end = 0
        for s in sent_list:
            tokens = self.tokenizer.tokenize(s)
            sent_end += len(tokens) 
            end_list.append(sent_end)
            if len(tokens) == 0:
                tokens = ['']
            text_ind += self.tokenizer.encode(tokens, add_special_tokens=False)

        pred_labels = []
        for i in range(len(sent_list) - 1):
            prompt, next_sentence = self._get_tokens_contexts(text_ind, end_list[i])
            encoding = self.tokenizer(prompt, next_sentence, return_tensors='pt')
            input_ids = encoding.input_ids.to(self.device)
            token_type_ids = encoding.token_type_ids.to(self.device)
            attention_mask = encoding.attention_mask.to(self.device)
            outputs = self.model(input_ids=input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids)
            logits = outputs.logits
            x = logits[0].detach().cpu().numpy()
            x_pp = softmax(x)
            if x_pp[0] > self.threshold:
                pred_labels.append(0)
            else:
                pred_labels.append(1)
        return pred_labels + [1]

    def _get_tokens_contexts(self, text_ind, end):
        width = self.width
        prompt = self.tokenizer.decode(text_ind[max(0, end - width):end], skip_special_tokens=True)
        next_sentence = self.tokenizer.decode(text_ind[end:min(len(text_ind), end + width)], skip_special_tokens=True)
        return prompt, next_sentence

    @staticmethod
    def split_sentences(text):
        return list(sentenize(text))