from segmentation.ModelsMap import models_map as segmentation_models_map

from logger.handlers import stdout_handler
import logging
logger = logging.Logger('predictor', level=logging.DEBUG)
logger.addHandler(stdout_handler())

class Predictor:

    @staticmethod
    def load_model(model_name):
        # загрузка сохраненнлй модели
        model = segmentation_models_map[model_name]()
        model.load_model()
        return model

    @staticmethod
    def prediction(model_name, text):
        # формирование предмказания
        pt_model = Predictor.load_model(model_name)
        predictions = pt_model.predict(text=text)
        logger.info(f'predictions: {predictions}')
        return predictions
