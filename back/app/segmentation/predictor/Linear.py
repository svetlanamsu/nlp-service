import torch

from .BaseModel import SimpleTorchModel
from ..models.LinearModel import LinearModel


class Linear(SimpleTorchModel):
    model_name = 'Линейная модель'
    def __init__(self):
        super().__init__()

    def load_model(self):
        model = LinearModel(embed_size=self.embed_size)
        model.load_state_dict(torch.load(f'segmentation/predictor/source/models/linear_state.pt', map_location=self.device))
        self.instance = model

    def predict(self, text):
        out = super().predict(text=text)
        return list(out)
