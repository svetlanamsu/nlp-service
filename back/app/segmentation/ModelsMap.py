from enum import Enum
from .predictor.Linear import Linear
from .predictor.HLSTMModel import HLSTMModel
from .predictor.ContextBert import ContextBertModel
from .predictor.BertLstmModel import BertLSTMModel

class ModelName(str, Enum):
    hlstm = "hlstm"
    linear = "linear"
    context_bert = "context_bert"
    bert_bilstm = "bert_bilstm"


models_map = {
    ModelName.linear: Linear,
    ModelName.hlstm: HLSTMModel,
    ModelName.context_bert: ContextBertModel,
    ModelName.bert_bilstm: BertLSTMModel
}
