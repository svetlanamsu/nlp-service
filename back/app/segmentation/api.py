import torch
import json

from .predictor.predictor import Predictor
from .ModelsMap import ModelName, models_map

from fastapi import APIRouter
from pydantic import BaseModel

from logger.handlers import stdout_handler
import logging
logger = logging.Logger('api', level=logging.DEBUG)
logger.addHandler(stdout_handler())


router = APIRouter()

class InputRawText(BaseModel):
    model: ModelName
    text: str

@router.get("/segmentation-server/")
def read_root():
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    torch_version = torch.__version__
    return {"torch_version": torch_version,
            "device": device}

@router.get("/segmentation-server/help")
def read_root():
    with open('segmentation/info.json') as json_file:
        data = json.load(json_file)
    return {"help": {
        "description": data["description"],
        "example": data["example"]
    }}


@router.get('/segmentation-server/get-models')
def show_models():
    ans = {'models': []}
    for k, i in models_map.items():
        item = {}
        item['name'] =  i.model_name 
        item['value'] = k.name
        logger.info(f'Item {item}')
        ans['models'].append(item)
    return ans


@router.post('/segmentation-server/prediction')
async def get_predictions(item: InputRawText):
    logger.info(f'Start generate predictions. Model = {item.model}')
    segments, label = Predictor.prediction(item.model, item.text)
    ans = {'segments': []}
    total = len(segments)
    for i, s in enumerate(segments, 1):
        ans['segments'].append(
            {
                'label': f'Сегмент {i}/{total}',
                'text': s 
            }
        )
    return ans



