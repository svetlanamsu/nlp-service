from transformers import T5Tokenizer, MT5ForConditionalGeneration
from torch import cuda
import torch
from .generate_summary import mT5_GenerateSummary

from logger.handlers import stdout_handler
import logging
logger = logging.Logger('Mt5Model', level=logging.DEBUG)
logger.addHandler(stdout_handler())

class modelMT5:

    model_name = 'mT5(лучшая)'

    def __init__(self):
        self.device = 'cuda' if cuda.is_available() else 'cpu'
        self.model_path = 'summarization/models/source/mt5_model'
        self.tokenizer = T5Tokenizer.from_pretrained(self.model_path)
        self.model = MT5ForConditionalGeneration.from_pretrained(self.model_path).to(self.device)
        self.max_length = 512
        self.min_length = 20
        self.seed = 42
        self.summary_max_length = 50
        self.strategy = 'cascade'

        torch.manual_seed(self.seed)
        torch.backends.cudnn.deterministic = True

        
    def prediction(self, text):
        logger.info(f'Start predictions. Text {text}')
        mt5 = mT5_GenerateSummary(
                device=self.device,
                tokenizer=self.tokenizer,
                model=self.model,
                max_length=self.max_length,
                min_length=self.min_length,
                seed=self.seed,
                summary_max_length=self.summary_max_length,
                strategy = self.strategy
            )
        
        summary = mt5.generate_summary(text=text)
        logger.info(f'Summary: {summary}')
        return summary
