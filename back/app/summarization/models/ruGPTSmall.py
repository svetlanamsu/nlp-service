from transformers import GPT2Tokenizer, GPT2LMHeadModel
from .generate_summary import GenerateSummary

from logger.handlers import stdout_handler
import logging
logger = logging.Logger('ruGPTSmall', level=logging.DEBUG)
logger.addHandler(stdout_handler())

class ruGPTSmall:
    model_name = 'Маленькая ruGPT'
    def __init__(self):
        self.model_path = 'summarization/models/source/summary_model_mos_v_0_2'
        self.model = GPT2LMHeadModel.from_pretrained(self.model_path)
        self.tokenizer = GPT2Tokenizer.from_pretrained(self.model_path)

    def prediction(self, text):
        gpt = GenerateSummary(
            tokenizer=self.tokenizer,
            model=self.model,
            bos_token='Текст:',
            eos_token='</s>',
            sum_token='Аннотация:',
            max_length=400
            )
        summary = gpt.generate_summary(text=text)
        return summary