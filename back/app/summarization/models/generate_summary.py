import re
from dataclasses import dataclass, field, asdict
from transformers import AutoTokenizer, AutoModel
from razdel import sentenize
import torch

from logger.handlers import stdout_handler
import logging
logger = logging.Logger('generate_summary', level=logging.DEBUG)
logger.addHandler(stdout_handler())

gpt_settings = {
    'temperature': 1.0,
    'top_k': 0,
    'top_p': .0,
    'repetition_penalty': 1.0,
    'do_sample': True,
    'num_return_sequences': 1,
}

settings = {
    'gpt': gpt_settings
}


class CoreGenerateSummary:
    def _clear(self, text):
        text = re.sub(r'[^А-Яа-яёЁ0-9 .-]', ' ', text)
        text = re.sub(r" +", " ", text)
        return text

    def generate_summary(self, text):
        # очистка теста для саммаризации
        empty_text = self._clear(text)
        self.source_text = empty_text
        # подготовка токенов для саммаризации
        inputs = self._spliting_text(empty_text)
        summary = self._predictions(inputs)
        return summary


@dataclass
class GenerateSummary(CoreGenerateSummary):
    """
    Класс для получения предсказаний по модели transformer
    """
    tokenizer: AutoTokenizer
    model: AutoModel
    max_length: int = 512
    min_length: int = 20
    sum_token: str = '<|sum|>'
    bos_token: str = '<|sos|>'
    eos_token: str = '<|eos|>'
    model_type: str = 'gpt'
    strategy: str = 'trancate'

    def _spliting_text(self, text):
        '''
        text: очищенный текст без специальных токенов
        return: возвращает токенизированный текст для подачи предсказаний
        '''
        input = self.bos_token + text + self.sum_token
        text_toks = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
        if len(text_toks[0]) < self.max_length:
            return text_toks
        # разделю текст по предложениям, соберу по пачкам и для каждой пачки сделаю саммари
        if self.strategy == 'trancate':
            return self._truncate_strategy(text)
        if self.strategy == 'cascade':
            return self._cascade_strategy(text)
        raise ValueError(f'Check valid strategy.')

    def _truncate_strategy(self, text_for_summary):
        '''
        text: очищенный текст для саммаризации
        стратегия при которой берутся первые предложения до набора в максимальное количество токенов
        '''
        output = ''
        sents = sentenize(text_for_summary)
        for i in sents:
            temp = output + i.text
            input = self.bos_token + temp + self.sum_token
            text_toks = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
            if len(text_toks[0]) > self.max_length:
                break
            output += i.text
        # если весь тект одно предложение
        if output == '':
            input = self.bos_token + text_for_summary + self.sum_token
            text_toks = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')[:, :self.max_length]
            return text_toks
        input = self.bos_token + output + self.sum_token
        text_toks = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
        return text_toks

    def _cascade_strategy(self, text_for_summary):
        '''
        Берем первых max_length токенов по ним делаем предсказание и вместо первых предложений подставляем саммари
        '''
        output = ''
        sents = sentenize(text_for_summary)
        for i in sents:
            temp = output + i.text
            input = self.bos_token + temp + self.sum_token
            text_toks = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
            if len(text_toks[0]) > self.max_length:
                input = self.bos_token + output + self.sum_token
                text_toks = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
                summary = self._predictions(text_toks)
                summary = summary + '.' if not summary.endswith('.') else summary
                output = summary
            output += i.text
        input = self.bos_token + output + self.sum_token
        return self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')

    def _predictions(self, input_token):
        '''
        input_token: токенизированный текст для саммаризации
        получение предсказаний по модели
        '''
        if len(input_token[0]) < self.min_length:
            return self.source_text
        # получение настроек для генерации саммари
        gen_settings = settings[self.model_type]
        output_sequence = self.model.generate(input_ids=input_token, max_length=512, **gen_settings)
        output_sequence = self.tokenizer.decode(output_sequence[0], clean_up_tokenization_spaces=True)
        output_sequence = output_sequence[: output_sequence.find(self.eos_token)]
        output_sequence = output_sequence[
                          len(self.tokenizer.decode(input_token[0], clean_up_tokenization_spaces=True)):]
        logger.info(f'Summary: {output_sequence}')
        return output_sequence.strip()



@dataclass
class mT5_GenerateSummary(CoreGenerateSummary):

    tokenizer: AutoTokenizer
    model: AutoModel
    max_length: int = 512
    min_length: int = 20
    summary_max_length: int = 50
    seed: int = 42
    device: str = 'cpu'
    strategy: str = 'truncate'

    def _spliting_text(self, text):
        inputs = self.tokenizer.encode(text, return_tensors="pt")
        
        if len(inputs[0]) < self.max_length:
            return inputs
        else:
            if self.strategy == 'truncate':
                return self._truncate_strategy(text)
            if self.strategy == 'cascade':
                return self._cascade_strategy(text)
        raise ValueError(f'Check valid strategy.')
        return inputs
    
    def _truncate_strategy(self, text):
        output = ''
        sents = sentenize(text)
        
        for i in sents:
            temp = output + i.text
            input = temp
            tokens = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
            if len(tokens[0]) > self.max_length:
                break
            output += i.text
        
        if output == '':
            input = text
            tokens = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')[:, :self.max_length]
            return tokens
        
        input = output
        tokens = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
        return tokens
    
    def _cascade_strategy(self, text):
        output = ''
        sents = sentenize(text)
        for i in sents:
            temp = output + i.text
            input = temp
            text_toks = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
            if len(text_toks[0]) > self.max_length:
                input = output
                text_toks = self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')
                summary = self._predictions(text_toks)
                summary = summary + '. ' if summary[-1] not in ['.', '!', '?'] else summary
                output = summary
            output += i.text
        input = output
        return self.tokenizer.encode(input, add_special_tokens=False, return_tensors='pt')


    def _predictions(self, inputs):  
        
        inputs = inputs.to(self.device, dtype=torch.long)
        outputs = self.model.generate(
                        input_ids = inputs,
                        max_length=self.summary_max_length
                        )
        
        summary = self.tokenizer.decode(
                outputs[0].tolist(),
                skip_special_tokens=True,
                clean_up_tokenization_spaces=True
        )
        return summary
