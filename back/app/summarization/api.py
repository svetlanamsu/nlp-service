from .ModelsMap import ModelName, models_map

import json
from fastapi import APIRouter
from pydantic import BaseModel

from logger.handlers import stdout_handler
import logging
logger = logging.Logger('summarization_api', level=logging.DEBUG)
logger.addHandler(stdout_handler())


router = APIRouter()

class InputRawText(BaseModel):
    model: ModelName
    text: str

@router.get("/summarization-server/")
def read_root():
    return {"info": "Эта модель предназначена для саммаризации."}

@router.get("/summarization-server/help")
def read_root():
    with open('summarization/info.json') as json_file:
        data = json.load(json_file)
    return {"help": {
        "description": data["description"],
        "example": data["example"]
    }}

@router.get('/summarization-server/get-models')
def show_models():
    ans = {'models': []}
    for k, i in models_map.items():
        item = {}
        item['name'] =  i.model_name 
        item['value'] = k.name
        logger.info(f'Item {item}')
        ans['models'].append(item)
    return ans


@router.post('/summarization-server/prediction')
def get_predictions(item: InputRawText):
    logger.info(f'Start generate predictions. Model = {item.model}')
    sum_model = models_map[item.model]()
    logger.info(f'Input text: {item.text}')
    summary = sum_model.prediction(text=item.text)
    ans = {'summary': summary}
    return ans



