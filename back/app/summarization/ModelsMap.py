from enum import Enum
from .models.ruGPTSmall import ruGPTSmall
from .models.Mt5Model import modelMT5

class ModelName(str, Enum):
    ruGPT_small = "ruGPT_small"
    mt5 = 'mt5'


models_map = {
    ModelName.ruGPT_small: ruGPTSmall,
    ModelName.mt5: modelMT5,
}
